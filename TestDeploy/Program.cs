﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using IC.Common;

namespace IC.Deploy
{
    public class Program
    {
        static void Main(string[] args)
        {
            XmlManager.getInstance().LoadDocument(@"C:\_lcudini\Dev\TestDeploy\TestDeploy\ICWorkflowConfig.xml", true);

            List<string> methods = new List<string>();

            methods = XmlManager.getInstance().ReadLinq();

            foreach (string method in methods)
            {
                Console.WriteLine(method);
            }
        }
    }
}
